package paddle_test

import (
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"testing"

	"code.soquee.net/paddle"
)

type roundTripperFunc func(req *http.Request) (*http.Response, error)

func (rt roundTripperFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return rt(req)
}

func mustReq(req *http.Request, err error) *http.Request {
	if err != nil {
		panic(err)
	}
	return req
}

func setURLEncoded(req *http.Request, err error) *http.Request {
	req = mustReq(req, err)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	return req
}

var rtTestCases = [...]struct {
	req *http.Request
	out string
}{
	// Don't set auth on non-POST methods
	0: {
		req: setURLEncoded(http.NewRequest("GET", "https://vendors.paddle.com/api/2.0", strings.NewReader(""))),
		out: "",
	},
	// Don't set auth on non-HTTPS
	1: {
		req: setURLEncoded(http.NewRequest("POST", "http://vendors.paddle.com/api/2.0", strings.NewReader("a=1&b=2"))),
		out: "a=1&b=2",
	},
	// Don't set auth if host not vendors.paddle.com
	2: {
		req: setURLEncoded(http.NewRequest("POST", "https://paddle.com/api/2.0", strings.NewReader("a=1&b=2"))),
		out: "a=1&b=2",
	},
	// Don't set auth if path not /api/version
	3: {
		req: setURLEncoded(http.NewRequest("POST", "https://paddle.com/api/1.0", strings.NewReader("a=1&b=2"))),
		out: "a=1&b=2",
	},
	// Don't set auth if not Content-Type: application/x-www-form-urlencoded
	4: {
		req: mustReq(http.NewRequest("POST", "https://vendors.paddle.com/api/2.0", strings.NewReader("a=1&b=2"))),
		out: "a=1&b=2",
	},
	// Must not append "&" if no existing body
	5: {
		req: setURLEncoded(http.NewRequest("POST", "https://vendors.paddle.com/api/2.0/test", strings.NewReader(""))),
		out: "vendor_auth_code=authcode&vendor_id=123",
	},
	// Must add "&" and append if existing body
	6: {
		req: setURLEncoded(http.NewRequest("POST", "https://vendors.paddle.com/api/2.0/test", strings.NewReader("a=1&b=2"))),
		out: "a=1&b=2&vendor_auth_code=authcode&vendor_id=123",
	},
}

func TestRoundTrip(t *testing.T) {
	for i, tc := range rtTestCases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			var out string
			rt := paddle.NewRoundTripper(roundTripperFunc(func(req *http.Request) (*http.Response, error) {
				buf, err := ioutil.ReadAll(req.Body)
				if err != nil {
					return nil, err
				}
				out = string(buf)
				return nil, nil
			}), 123, "authcode")

			_, err := rt.RoundTrip(tc.req)
			if err != nil {
				t.Errorf("Error while round tripping: %q", err)
			}
			if out != tc.out {
				t.Errorf("Unexpected output: want=%q, got=%q", tc.out, out)
			}
		})
	}
}

func TestDoubleWrap(t *testing.T) {
	rt := paddle.NewRoundTripper(roundTripperFunc(func(req *http.Request) (*http.Response, error) {
		return &http.Response{Body: req.Body}, nil
	}), 123, "authcode")
	rt2 := paddle.NewRoundTripper(rt, 456, "newauthcode")
	req := setURLEncoded(http.NewRequest("POST", "https://vendors.paddle.com/api/2.0", strings.NewReader("a=1&b=2")))
	resp, err := rt2.RoundTrip(req)
	if err != nil {
		t.Fatalf("Unexpected error: %q", err)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Unexpected error reading form test RoundTripper: %q", err)
	}

	const expected = "a=1&b=2&vendor_auth_code=authcode&vendor_id=123"
	if s := string(b); s != expected {
		t.Errorf("Unexpected output: want=%q, got=%q", expected, s)
	}
}

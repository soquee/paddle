package coupon

// DiscountType is a type of discount that can be applied with a coupon.
type DiscountType string

// A list of valid coupon types.
const (
	DiscountFlat       DiscountType = "flat"
	DiscountPercentage DiscountType = "percentage"
)

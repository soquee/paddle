// Package coupon provides APIs related to coupons.
package coupon

import (
	"bytes"
	"encoding/json"
	"net/url"
	"strconv"
	"strings"
	"time"

	"code.soquee.net/paddle"
	"code.soquee.net/paddle/internal"
)

const (
	createCouponsURL = "/product/create_coupon"
	listCouponsURL   = "/product/list_coupons"
	updateCouponsURL = "/product/update_coupon"
	deleteCouponsURL = "/product/delete_coupon"
)

// Client performs API operations.
type Client struct {
	*paddle.Client
}

// List requests a list of coupons available for the given product and returns
// an iterator that decodes them from the response body.
//
// If an error is returned, it is decoded and returned immediately.
func (c Client) List(productID int64) (Iter, error) {
	v := url.Values{}
	v.Add("product_id", strconv.FormatInt(productID, 10))

	msg, err := c.Do(listCouponsURL, v)
	if err != nil {
		return Iter{}, err
	}
	return Iter{
		d: json.NewDecoder(bytes.NewReader(msg)),
	}, nil
}

// Create creates a number of identical coupons.
// If num is greater than 1, coupon.Code is ignored and the codes are
// randomized.
// The coupon.Product field is ignored.
func (c Client) Create(num int, prefix, group string, couponType CouponType, recurring bool, coupon *Coupon) ([]string, error) {
	v := url.Values{}
	if num <= 1 {
		if coupon.Code != "" {
			v.Add("coupon_code", prefix+coupon.Code)
		}
	} else {
		v.Add("num_coupons", strconv.Itoa(num))
	}
	if prefix != "" && coupon.Code == "" {
		v.Add("coupon_prefix", prefix)
	}
	v.Add("description", coupon.Description)
	v.Add("coupon_type", string(couponType))
	var sb strings.Builder
	for _, product := range coupon.Product {
		if sb.Len() > 0 {
			sb.WriteByte(',')
		}
		/* #nosec */
		sb.WriteString(strconv.FormatInt(product, 10))
	}
	v.Add("product_ids", sb.String())
	v.Add("discount_type", string(coupon.DiscountType))
	v.Add("discount_amount", strconv.FormatFloat(coupon.DiscountAmount, 'f', -1, 64))
	if coupon.Currency != "" {
		v.Add("currency", string(coupon.Currency))
	}
	v.Add("allowed_uses", strconv.Itoa(coupon.AllowedUses))
	v.Add("expires", time.Time(coupon.Expires).Format(internal.DateFmt))
	// TODO: paddle claims that when creating this you should use "0" or "1"… WTF?
	// See: https://paddle.com/docs/api-generate-coupon/
	v.Add("recurring", strconv.FormatBool(coupon.Recurring))
	v.Add("minimum_threshold", strconv.FormatFloat(coupon.Minimum, 'f', -1, 64))
	if group != "" {
		v.Add("group", group)
	}

	msg, err := c.Do(createCouponsURL, v)
	if err != nil {
		return nil, err
	}
	codes := []string{}
	if num > 0 {
		err = json.Unmarshal(msg, codes)
	}
	return codes, err
}

// Update updates properties of a single coupon.
//
// Empty values will be ignored.
// If products is nil, it will be ignored, if it is an empty slice, all products
// will be removed.
func (c Client) Update(code string, coupon *Coupon) error {
	return c.updateCoupons("coupon_code", code, coupon.Code, coupon)
}

// UpdateGroup updates properties of an entire group of coupons.
//
// See UpdateCoupon for more information.
func (c Client) UpdateGroup(group, newGroup string, coupon *Coupon) error {
	return c.updateCoupons("code", group, newGroup, coupon)
}

func (c Client) updateCoupons(typ, code, newCode string, coupon *Coupon) error {
	v := url.Values{}
	v.Add(typ, code)
	if newCode != "" {
		v.Add("new_"+typ, newCode)
	}
	if coupon.Product != nil {
		var sb strings.Builder
		for _, product := range coupon.Product {
			if sb.Len() > 0 {
				sb.WriteByte(',')
			}
			/* #nosec */
			sb.WriteString(strconv.FormatInt(product, 10))
		}
		v.Add("product_ids", sb.String())
	}
	if !time.Time(coupon.Expires).IsZero() {
		v.Add("expires", time.Time(coupon.Expires).Format(internal.DateFmt))
	}
	if coupon.AllowedUses > 0 {
		v.Add("allowed_uses", strconv.Itoa(coupon.AllowedUses))
	}
	if coupon.DiscountType != "" {
		if coupon.DiscountType == DiscountFlat {
			v.Add("currency", string(coupon.Currency))
		}
		v.Add("discount_amount", strconv.FormatFloat(coupon.DiscountAmount, 'f', -1, 64))
	}

	_, err := c.Do(updateCouponsURL, v)
	return err
}

// Delete deletes the given coupon from the given product.
func (c Client) Delete(product int64, code string) error {
	v := url.Values{}
	v.Add("product_id", strconv.FormatInt(product, 64))
	v.Add("coupon_code", code)

	_, err := c.Do(deleteCouponsURL, v)
	return err
}

// Iter provides a mechanism for lazily decoding and iterating over a list of
// coupons.
type Iter struct {
	d *json.Decoder
}

// Next returns true if there are more coupons to decode.
func (c Iter) Next() bool {
	return c.d.More()
}

// Decode returns the next coupon.
func (c Iter) Decode() (*Coupon, error) {
	coupon := &Coupon{}
	err := c.d.Decode(coupon)
	return coupon, err
}

// Coupon represents a single coupon for a specific product.
type Coupon struct {
	ID             int64           `json:"id"`
	Code           string          `json:"coupon"`
	Description    string          `json:"description"`
	DiscountType   DiscountType    `json:"discount_type"`
	DiscountAmount float64         `json:"discount_amount"`
	Currency       paddle.Currency `json:"discount_currency"`
	AllowedUses    int             `json:"allowed_uses"`
	TimesUsed      int             `json:"times_used"`
	Recurring      bool            `json:"is_recurring"`
	Expires        paddle.Time     `json:"expires"`
	Product        []int64         `json:"-"`
	Minimum        float64         `json:"minimum_threshold"`
}

var _ json.Unmarshaler = (*Coupon)(nil)

// UnmarshalJSON satisfies the json.Unmarshaler interface for Coupon.
//
// It is necessary because when creating coupons a slice of product IDs is
// required, but when getting coupons an individual product ID is provided.
// When unmarshaling, convert the indivdiual product ID into a slice of length
// 1.
func (c *Coupon) UnmarshalJSON(b []byte) error {
	newC := struct {
		Coupon
		P int64 `json:"product_id"`
	}{}
	err := json.Unmarshal(b, &newC)
	if err != nil {
		return err
	}

	c.Product = []int64{newC.P}
	return nil
}

// CouponType is a specific type of coupon.
type CouponType string

// A list of valid coupon types.
const (
	CouponProduct  CouponType = "product"
	CouponCheckout CouponType = "checkout"
)

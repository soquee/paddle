// Package paddle is an HTTP API client for the Paddle payment processing service.
//
// Be advised
//
// This package is unmaintained. It does not receive support or security
// updates. Please do not attempt to use it in production.
package paddle

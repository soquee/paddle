// Package product provides APIs related to products.
package product

import (
	"bytes"
	"encoding/json"

	"code.soquee.net/paddle"
)

// Client performs API operations.
type Client struct {
	*paddle.Client
}

const listProductsURL = "/product/get_products"

// List requests a list of proucts available on your account and returns an
// iterator that decodes them from the response body.
// If an error is returned, it is decoded and returned immediately.
func (c Client) List() (Iter, error) {
	msg, err := c.Do(listProductsURL, nil)
	if err != nil {
		return Iter{}, err
	}
	pd := &productsDecode{}
	err = json.Unmarshal(msg, pd)
	if err != nil {
		return Iter{}, err
	}
	return Iter{
		Total: pd.Total,
		Count: pd.Count,
		d:     json.NewDecoder(bytes.NewReader(pd.Products)),
	}, nil
}

type productsDecode struct {
	Total    int             `json:"total"`
	Count    int             `json:"count"`
	Products json.RawMessage `json:"products"`
}

// Iter provides a mechanism for lazily decoding and iterating over a list of
// products.
type Iter struct {
	Total int
	Count int
	d     *json.Decoder
}

// Next returns true if there are more products to decode.
func (p Iter) Next() bool {
	return p.d.More()
}

// Decode returns the next product.
func (p Iter) Decode() (*Product, error) {
	prod := &Product{}
	err := p.d.Decode(prod)
	return prod, err
}

// Product contains data from a single product.
type Product struct {
	ID          uint64   `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	BasePrice   float64  `json:"base_price"`
	SalePrice   *int     `json:"sale_price"`
	Screenshots []string `json:"screenshots"`
	Icon        string   `json:"icon"`
}

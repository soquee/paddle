// Package payment provides APIs related to payments.
package payment

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"code.soquee.net/paddle"
	"code.soquee.net/paddle/internal"
)

const (
	listURL       = "/subscription/payments"
	rescheduleURL = "/subscription/payments_reschedule"
)

// Client performs API operations.
type Client struct {
	*paddle.Client
}

// Reschedule changes the due date on an upcoming payment and all future
// payments on the same schedule.
func (c Client) Reschedule(id uint64, date time.Time) error {
	v := url.Values{}
	v.Add("payment_id", strconv.FormatUint(id, 10))
	v.Add("date", date.Format(internal.DateFmt))
	_, err := c.Do(rescheduleURL, v)
	return err
}

// List returns an iterator over all payments for the logged in user, filtered
// by the given arguments.
func (c Client) List(plans []uint64, subscription uint64, paid bool, oneOff bool, from, to time.Time) (Iter, error) {
	v := url.Values{}
	if len(plans) > 0 {
		var b strings.Builder
		for i, plan := range plans {
			if i > 0 {
				b.WriteByte(',')
			}
			fmt.Fprintf(&b, "%d", plan)
		}
		v.Add("plan", b.String())
	}
	if subscription != 0 {
		v.Add("subscription_id", strconv.FormatUint(subscription, 10))
	}
	// TODO: the docs claim these should be the number 0 or 1… but other things
	// use bools correctly, and yet others claim to use the strings "0" and "1".
	v.Add("is_paid", strconv.FormatBool(paid))
	v.Add("is_one_off_charge", strconv.FormatBool(oneOff))

	// TODO: these parameters aren't documented, but are in one of the examples.
	// Do they even exist? Are they dates or datetimes?
	if !from.IsZero() {
		v.Add("from", from.Format(internal.DateFmt))
	}
	if !to.IsZero() {
		v.Add("to", to.Format(internal.DateFmt))
	}

	msg, err := c.Do(listURL, v)
	if err != nil {
		return Iter{}, err
	}
	return Iter{
		d: json.NewDecoder(bytes.NewReader(msg)),
	}, nil
}

// Payment may represent an existing payment or an upcoming (unpaid) payment.
type Payment struct {
	ID           int64           `json:"id"`
	Subscription int64           `json:"subscription_id"`
	Amount       uint            `json:"amount"`
	Currency     paddle.Currency `json:"currency"`
	PayoutDate   paddle.Time     `json:"payout_date"`
	Paid         bool            `json:"is_paid"`
	OneOff       bool            `json:"is_one_off_charge"`
	Receipt      string          `json:"receipt_url"`
}

// Iter provides a mechanism for lazily decoding and iterating over a list.
type Iter struct {
	d *json.Decoder
}

// Next returns true if there is more data to decode.
func (c Iter) Next() bool {
	return c.d.More()
}

// Decode returns the next payment.
func (c Iter) Decode() (*Payment, error) {
	payment := &Payment{}
	err := c.d.Decode(payment)
	return payment, err
}

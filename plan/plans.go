// Package plan provides APIs related to plans.
package plan

import (
	"bytes"
	"encoding/json"
	"net/url"
	"strconv"
	"strings"

	"code.soquee.net/paddle"
)

const (
	listPlansURL   = "/subscription/plans"
	createPlansURL = "/subscription/plans_create"
)

// Client performs API operations.
type Client struct {
	*paddle.Client
}

// Get returns a single plan by its ID.
func (c Client) Get(plan uint64) (*Plan, error) {
	iter, err := c.list(int64(plan))
	if err != nil {
		return nil, err
	}
	return iter.Decode()
}

// List returns an iterator over all of the existing plans.
func (c Client) List() (Iter, error) {
	return c.list(-1)
}

func (c Client) list(plan int64) (Iter, error) {
	v := url.Values{}
	if plan >= 0 {
		v.Add("plan", strconv.FormatInt(plan, 10))
	}
	msg, err := c.Do(listPlansURL, v)
	if err != nil {
		return Iter{}, err
	}
	return Iter{
		d: json.NewDecoder(bytes.NewReader(msg)),
	}, nil
}

// Create makes a new plan.
func (c Client) Create(plan *Plan) (id uint64, err error) {
	v := url.Values{}
	v.Add("plan_type", string(plan.BillingType))
	v.Add("plan_length", strconv.FormatUint(uint64(plan.BillingPeriod), 10))
	v.Add("plan_trial_days", strconv.FormatUint(uint64(plan.TrialDays), 10))

	currency := string(plan.MainCurrency)
	v.Add("main_currency_code", currency)

	currency = strings.ToLower(currency)
	v.Add("recurring_price_"+strings.ToLower(currency), strconv.FormatFloat(plan.RecurringPrice[plan.MainCurrency], 'f', -1, 64))

	// TODO: is this even a thing? The main_currency_code docs mention it, but the parameters list doesn't contain it.
	v.Add("initial_price_"+strings.ToLower(currency), strconv.FormatFloat(plan.InitialPrice[plan.MainCurrency], 'f', -1, 64))

	msg, err := c.Do(createPlansURL, v)
	if err != nil {
		return 0, err
	}
	planResp := struct {
		ID uint64 `json:"product_id"`
	}{}
	err = json.Unmarshal(msg, &planResp)
	return planResp.ID, err
}

// Iter provides a mechanism for lazily decoding and iterating over a list of
// plans.
type Iter struct {
	d *json.Decoder
}

// Next returns true if there are more plans to decode.
func (c Iter) Next() bool {
	return c.d.More()
}

// Decode returns the next plan.
func (c Iter) Decode() (*Plan, error) {
	plan := &Plan{}
	err := c.d.Decode(plan)
	return plan, err
}

// BillingType represents a frequency at which the plan can be billed.
type BillingType string

// A list of acceptable billing types.
const (
	BillingDaily   = "day"
	BillingWeekly  = "week"
	BillingMonthly = "month"
	BillingYearly  = "year"
)

// Plan represents a subscription plan.
type Plan struct {
	ID             uint64                      `json:"id"`
	Name           string                      `json:"name"`
	BillingType    BillingType                 `json:"billing_type"`
	BillingPeriod  uint                        `json:"billing_period"`
	InitialPrice   map[paddle.Currency]float64 `json:"initial_price"`
	RecurringPrice map[paddle.Currency]float64 `json:"recurring_price"`
	TrialDays      uint                        `json:"trial_days"`

	// Used to pick an InitialPrice and RecurringPrice when creating a plan.
	// Not part of the plan list response.
	MainCurrency paddle.Currency `json:"-"`
}

package internal

// Constants used internally by the various subpackages.
const (
	DateFmt     = "2006-01-02"
	DateTimeFmt = "2006-01-02 15:04:05"
)

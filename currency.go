package paddle

// Currency is an ISO 4217 currency designator.
type Currency string

// Reexport of currencies currently supported by Paddle.
const (
	// G10 currencies
	USD Currency = "USD"
	EUR Currency = "EUR"
	GBP Currency = "GBP"
)

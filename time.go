package paddle

import (
	"encoding/json"
	"time"

	"code.soquee.net/paddle/internal"
)

// Time is a custom time type that supports marshaling and unmarshaling Paddle's
// custom time format that ignores the standard JSON format.
type Time time.Time

var _ json.Unmarshaler = (*Time)(nil)

// UnmarshalJSON satisfies the json.Unmarshaler interface for Time.
func (t *Time) UnmarshalJSON(b []byte) error {
	newTime, err := time.Parse(internal.DateFmt, string(b))
	if err != nil {
		newTime, err = time.Parse(internal.DateTimeFmt, string(b))
		if err != nil {
			return err
		}
	}

	*t = Time(newTime)
	return nil
}

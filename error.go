package paddle

// Error can unmarshal the "error" field of an API response.
type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// Error implements the error interface for the Error type.
func (err Error) Error() string {
	return err.Message
}

package paddle

import (
	"encoding/json"
	"net/http"
	"net/url"
	stdpath "path"
)

// NewClient returns a Paddle API client that wraps an existing HTTP client and
// adds auth support for all POST requests to the API endpoint.
func NewClient(c *http.Client, vendorID int, vendorAuthCode string) *Client {
	c.Transport = NewRoundTripper(c.Transport, vendorID, vendorAuthCode)
	return &Client{
		c: c,
	}
}

// Client is a paddle HTTP API client.
type Client struct {
	c *http.Client
}

// Do sends an HTTP POST request to the server and decodes common response
// fields.
func (c *Client) Do(path string, v url.Values) (json.RawMessage, error) {
	type commonResponse struct {
		Success  bool            `json:"success"`
		Err      Error           `json:"error,omitempty"`
		Response json.RawMessage `json:"response,omitempty"`
	}

	resp, err := c.c.PostForm(stdpath.Join(baseURL, path), v)
	if err != nil {
		return nil, err
	}
	r := &commonResponse{}
	err = json.NewDecoder(resp.Body).Decode(r)
	if err != nil {
		return nil, err
	}
	if r.Success == false {
		return nil, r.Err
	}
	return r.Response, err
}

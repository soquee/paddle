// Package license provides APIs related to licenses.
package license

import (
	"encoding/json"
	"net/url"
	"strconv"
	"time"

	"code.soquee.net/paddle"
	"code.soquee.net/paddle/internal"
)

const createLicenseURL = "/product/generate_license"

// Client performs API operations.
type Client struct {
	*paddle.Client
}

// CreateLicense generates a new license code for a given SDK product.
func (c Client) Create(productID int64, allowedUses int, expiresAt time.Time) (*License, error) {
	v := url.Values{}
	v.Add("product_id", strconv.FormatInt(productID, 10))
	v.Add("allowed_uses", strconv.Itoa(allowedUses))
	v.Add("expires_at", expiresAt.Format(internal.DateFmt))
	msg, err := c.Do(createLicenseURL, v)
	if err != nil {
		return nil, err
	}
	l := &License{}
	err = json.Unmarshal(msg, l)
	if err != nil {
		return nil, err
	}
	return l, err
}

// License represents a license for an SDK product.
type License struct {
	Code      string      `json:"license_code"`
	ExpiresAt paddle.Time `json:"expires_at"`
}
